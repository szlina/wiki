# Eventesia Development

## Docker

### Build Images

```shell
docker-compose build
```

### Start Containers

```shell
docker-compose up -d
```

### Stop Containers

```shell
docker-compose down
```

### Print Logs
```shell
docker-compose logs -f
```

### List Running Images
```shell
docker ps
```

### Open in browser
http://localhost:5000

### Push docker images

```shell
docker login -u login_name

docker tag image_name:latest login_name/image_name:latest
docker push login_name/image_name:latest
```

### Pull docker images

```shell
docker pull login_name/image_name:latest
```

## MySQL

### Login to DB
```shell
docker-compose exec db mysql --host=db --user=user_name --password=the_password image_name
```

## App
### Run Command
```shell
docker-compose exec backend <command>
```

## GIT
### Submodule Update
```shell
git submodule update --init --recursive
```

## Testing

### Build test image

```shell
docker-compose -f docker-compose.test.yaml build
```

### Run tests

Run all tests:

```shell
docker-compose -f docker-compose.test.yaml run test
```

Run specific type of test:

```shell
docker-compose -f docker-compose.test.yaml run test static|unit|e2e|...
```

Debug container:

```shell
docker-compose -f docker-compose.test.yaml run test debug
```
