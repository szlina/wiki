# Ubuntu

## Useful commands

Last command exit status: `$ echo $?`

Laptop serial: `$ sudo dmidecode -s system-serial-number`

Samba mount: `$ sudo mount -t cifs //share/devel /home/samba -o rw,defaults,user=ksz,vers=3.0`

Tar - todo!
`$ tar xzf`

Endless loop: `$ while true; do command; sleep 5; done`

## SSH
Tunneling:

`$ ssh -i path/to/key gu=login1@login2@url -p jump_port -CN -L my_port:ip:target_port`

`$ ssh -i path/to/key -p jump_port root@localhost`

## VNC
Install: `$ sudo apt install tigervnc-viewer`
    
### Server side
Create: `$ vncserver :7 -localhost -geometry 1840x1000 -depth 32`

List: `$ vncserver -list`

What port? `$ netstat -tulnp | grep vnc`

Delete: `$ vncserver --kill :7`

### Client side
Tunnel: `$ ssh -L 5907:127.0.0.1:5907 -N -f server_ip`

Connect: `$ vncviewer localhost:5907`
