# xPath
## Firefox console
$x('//...')

## Syntax
| selector | xPath |
| ----------- | ----------- |
| h1 | //h1 |
| div p | //div//p |
| ul > li > a | //ul/li/a |
| div > * | //div/* |
| #id | //*[\@id="id"] |
| .class | //*[\@class="class"] |
| input[type="submit"] | //input[\@type="submit"] |

## Useful links
https://www.w3schools.com/xml/xpath_syntax.asp

https://docs.marklogic.com/guide/xquery/xpath

https://devhints.io/xpath
