# Git
## Branch stuff
### New branch, keep modifications
$ git checkout -b *new-branch-name*

### What is the upstream?
$ git branch -vv

### What are the remote repositories?
$ git remote -v

### Delete local branch
$ git branch -d *branch-name*

### Clean up, and refresh to top
$ git fetch origin; git reset --hard origin/*branch-name*

### Commit (last) move from wrong branch to correct branch
$ git reset --soft HEAD^

chackout good branch, than commit

### Branch history cleanup
$ git checkout master

$ git pull

$ gv | grep 'origin/.*: gone]'

$ git branch -vv | grep 'origin/.*: gone]' | awk '{print $1}' | xargs git branch -d

## Revert and reset
### Reset branch
$ git reset --hard

### Reset file
$ git checkout -- *filename*

### Revert add
$ git reset file


## Commit stuff
### Edit commits
$ git rebase -i origin/master

### Commit split up
$ git rebase -i origin/master

write before commit: e (edit)

$ git reset HEAD~

...add, commit - how we want...

After it's cool:

$ git rebase --continue

## Push stuff
### No upstream
$ git push -u origin *branch-name*

### Force push
$ git push -f

## Stash
### Save
$ git stash
$ git stash save "my_stash"

### What is in stash?
#### List
$ git stash list

### Diff
$ git stash show -p

$ git stash show -p stash@{n}

### Apply stash
#### Apply and delete
$ git stash pop

$ git stash pop stash@{n}

#### Apply and keep
$ git stash apply

$ git stash apply stash@{n}

### Delete
#### One item
$ git stash drop stash@{n}

#### Everything
$ git stash clear

## Git diff
### Diff between current branch and master:
$ git diff master

### Diff between two branches, e.g. master and staging:
$ git diff master..staging

### Show only files that are different between the two branches (without changes themselves):
$ git diff --name-status master..staging

## Useful stuff
### Submodule update
$ git submodule update --init --recursive

### Modifications in file by string - even if it is deleted
$ git log -S *string* *path/to/file*

### Tagging
$ git tag -a v1.0 -m 'version 1.0'

### Show all remote branches
$ git remote -v
