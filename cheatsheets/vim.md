# VI
## Replace string
:%s/old/new/g

## Goto line
42G
42gg

## Refresh all tabs
:bufdo e

## ctags
ctags --langmap=python:+.zts -R -f .ctags .

## Remove whitespaces from blank lines
:g/^\s\+$/s/\s\+//

:%s/^\s\+$//
