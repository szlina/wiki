# Docker
## Docker compose
$ docker-compose run name ...

## Pull & run
docker pull image:label && docker run --rm -v path:/mnt/folder -ti image:label
    
## Image build
### Personal image
docker build -t kakao Dockerfiles/something/

### Public image
docker build -t "registry/team/image:tag" Dockerfiles/something/

docker login registry/team

docker push "registry/team/image:tag"


