# Instroduction

This is my basic portable working environment:
- OS: Ubuntu 20.04 LTS (Focal Fossa)
- IDE: Vim with a lot of beautiful plugins
- Git: I keep my codes and stuff in git
- Docker: My stuff running in Dockerized Containers
- Some useful packages like: tmux, curl, etc.

I prefer work on Ubuntu Linux - but sometimes I need to use Windows, so I figured out the most comfortable way to make an Ubuntu environment inside Windows.

# Set-up the default working environment

## On GNU/Linux

### Common

1. Install tmux (sudo apt install tmux)
2. Go to home directory (cd ~) and check the following:
- Check if .bash_aliases, .bashrc and .tmux exists.
- If not exists, than you can copy this version into home directory from here.
- If it exists, than you should merge the two versions very carefully!

### Other

Visit subfolders, and follow instructions to setup other stuff

## On Windows

### Prerequisites
1. Check Vagrant-VirtualBox compatibility: https://www.vagrantup.com/docs/providers/virtualbox
2. Download and install Virtualbox: https://www.virtualbox.org/
3. Download and install Vagrant: https://www.vagrantup.com/
4. Download and install Putty: https://www.putty.org/
5. Download and install Puttygen: https://www.puttygen.com/

### Set-up Vagrant-Virtualbox environment

1. Start machine: `vagrant up`
2. Convert key for Windows Putty:
	1. Start puttygen
	2. Load ./.vagrant/machines/*current machine*/virtualbox/private_key
	3. Save the private key with the “.ppk” extension
3. Set putty connection:
	- IP address: 192.168.63.63
	- Connection/Data > Auto-login username: vagrant
	- Connection/SSH/Auth > Private key file...: set the generated ppk file
	- Save Session

### Usage
**Start:**
1. Start machine: `vagrant up`
2. Open Putty Session

**Stop:**
Power off machine: `vagrant halt`

**Advanced:**

Connect to machine from cmd: `vagrant ssh`

Hibernate machine: `vagrant suspend`

Destroy machine: `vagrant destroy`

Remove image:
`vagrant box list`
`vagrant box remove <box_name>`

Start new Vagrant project: `vagrant init`

Run provisions (after any modification):
 - Reload: `vagrant reload`
 - Run and force provision: `vagrant up --provision`

