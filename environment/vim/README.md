# Basic setup for Vim editor

## Initial setup
Copy .dir_colors and .vimrc into home

```
$ cp .dir_colors ~/.dir_colors
$ cp .vimrc ~/.vimrc
```

Add these lines to .bashrc:

```
if [ -f "/etc/DIR_COLORS" ]
then
  eval $(dircolors -b /etc/DIR_COLORS)
  if [ -f "$HOME/.dir_colors" ]
  then
    eval $(dircolors -b $HOME/.dir_colors)
  fi
fi
```

If .vim or its folders are not exist, make them:

```
$ mkdir ~/.vim
$ mkdir ~/.vim/autoload
$ mkdir ~/.vim/bundle
```

Run:

```
$ ./download_vim_plugins.sh
```

## Maintainance steps
TODO:
- .vimrc
- .bashrc
- setup_vim_plugins.sh

