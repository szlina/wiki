" Pathogen install
runtime bundle/vim-pathogen/autoload/pathogen.vim
call pathogen#infect()
call pathogen#helptags()

" enable project Specific .vimrc Files - make sure you add 'set secure' to the end of this file
set exrc

" Redefine leader key
let mapleader = "\<Space>"

" Set UTF-8 encoding
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8

" Disable vi compatibility (emulation of old bugs)
set nocompatible

" Color options
let g:solarized_use16 = 0
set background=dark
colorscheme solarized8
hi CursorLine   cterm=NONE ctermbg=237 ctermfg=NONE guibg=darkred guifg=white

" Save swap files into one place
set directory^=$HOME/.vim/swap//

" Airline
let g:airline_powerline_fonts = 1
let g:airline_theme='bubblegum'

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" Viewer options
set cursorline
set cursorcolumn
set showmatch " highlight matching braces
set comments=sl:/*,mb:\ *,elx:\ */ " intelligent comments
set list listchars=tab:»·,trail:· "list whitespaces
set ruler
set number
set scrolloff=5 "cursor will stay 5 lines away from edge
" set autowrite
set diffopt+=iwhite "ignore whitespace in diff

" Buftabline
set hidden
nnoremap <C-N> :bnext<CR>
nnoremap <C-P> :bprev<CR>
let g:buftabline_numbers = 2
nmap <leader>1 <Plug>BufTabLine.Go(1)
nmap <leader>2 <Plug>BufTabLine.Go(2)
nmap <leader>3 <Plug>BufTabLine.Go(3)
nmap <leader>4 <Plug>BufTabLine.Go(4)
nmap <leader>5 <Plug>BufTabLine.Go(5)
nmap <leader>6 <Plug>BufTabLine.Go(6)
nmap <leader>7 <Plug>BufTabLine.Go(7)
nmap <leader>8 <Plug>BufTabLine.Go(8)
nmap <leader>9 <Plug>BufTabLine.Go(9)
nmap <leader>0 <Plug>BufTabLine.Go(10)

nnoremap <Leader>b :ls<CR>:b<Space>
nnoremap <Leader>p :b<Space>#<CR>

nmap <S-Enter> O<Esc>
nmap <CR> o<Esc>

" Indent settings
filetype plugin indent on
set tabstop=4        " tab width is 4 spaces
set shiftwidth=4     " indent also with 4 spaces
set expandtab        " expand tabs to spaces

" Syntax options
syntax on

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Default language settings. We can override this by project
let g:syntastic_python_checkers = ['flake8']
let g:syntastic_sh_checkers = ["shellcheck"]
let g:syntastic_php_checkers = ["php", "phpcs", "phpmd"]
let g:syntastic_javascript_checkers = ["eslint", "jslint"]
let g:pymode_lint_ignore = "E501,W"
let g:syntastic_python_flake8_args = "--max-line-length=120"

" Close syntastic error plane before buffertab closed
nnoremap <silent> <C-d> :lclose<CR>:bdelete<CR>
cabbrev <silent> bd <C-r>=(getcmdtype()==#':' && getcmdpos()==1 ? 'lclose\|bdelete' : 'bd')<CR>

autocmd BufRead,BufNewFile *.yaml set filetype=ansible

" Project related syntax checking has moved to projects' root .vimrc.
    " Usage:
    " For zts behaves like py:
        " autocmd BufRead,BufNewFile *.zts set filetype=python
    " read setup.cfg file - and max-line-length if needed
        " let g:syntastic_python_flake8_args = "--config=setup.cfg --max-line-length=120"


" see :h syntastic-loclist-callback
function! SyntasticCheckHook(errors)
    if !empty(a:errors)
        let g:syntastic_loc_list_height = min([len(a:errors), 10])
    endif
endfunction

" Enable mouse usage
set mouse=a

" Key bindings
set pastetoggle=<F9>
" Save with F2
nmap <silent><F2> :w<CR>
imap <silent><F2> <ESC>:w<CR>i
" nmap <silent><F2> :%s/^\s\+$//<CR>:w<CR>:noh<CR>
" imap <silent><F2> <ESC>:%s/^\s\+$//<CR>:w<CR>:noh<CR>i
" No highlight
nmap <silent><F3> :noh<CR>
" Delete, but no change to the registers.
noremap <Leader>d "_d
" Toggle tagbar
nmap <F6> :TagbarToggle<CR>

map <Leader>i oimport pdb; pdb.set_trace()  # noqa: E702<ESC>

" Search options
set ignorecase
set smartcase
set incsearch        " Incremental search
set hlsearch

" Tag settings
set tags=./.ctags;/
let g:tagbar_width=25

if &diff
    " nothing?
else
    autocmd FileType * call tagbar#autoopen(0)
    let g:tagbar_autofocus = 0
endif

set laststatus=2

" Session save
set viminfo='10,\"100,:20,%,n~/.viminfo
set shortmess+=I

" nnoremap <leader>s :ToggleWorkspace<CR>
" let g:workspace_session_name = 'LinaSession.vim'
" let g:workspace_session_directory = $HOME . '/.vim/sessions/'
" let g:workspace_session_disable_on_args = 1

" Show margin at 120 characters
:set colorcolumn=+1
:highlight ColorColumn ctermbg=black guibg=black
:set colorcolumn=120

" enable project Specific .vimrc Files
set secure
