#!/bin/bash

set -e

function print_message()
{
  echo "####################"
  echo $1
  echo "####################"
}

function create_empty_dir()
{
    DIR=$1
    print_message "Create $DIR directory."
    mkdir $DIR
}

function create_directories()
{
    create_empty_dir "~/.vim"
    create_empty_dir "~/.vim/autoload"
    create_empty_dir "~/.vim/bundle"
}

function install_prerequisites()
{
    print_message "Install curl"
    sudo apt-get install curl

    print_message "Install pathogen"
    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

    print_message "Install Ctags"
    sudo apt-get install ctags
}

function install_vim_plugins()
{
    cd ~/.vim/bundle

    print_message "Install vim-solarized8 - Fancy theme"
    git clone https://github.com/lifepillar/vim-solarized8

    print_message "Install vim-airline - Lean & mean status/tabline"
    git clone https://github.com/vim-airline/vim-airline
    git clone https://github.com/vim-airline/vim-airline-themes

    print_message "Install syntastic - Syntax checker (flake8, ...)"
    git clone https://github.com/scrooloose/syntastic

    print_message "Install ctrlp - Find files and folders on CTRL+P"
    git clone https://github.com/vim-scripts/ctrlp.vim

    print_message "Install tagbar - Function list and code navigation. Requires ctags"
    git clone https://github.com/majutsushi/tagbar

    print_message "Install TaskList - List TODOs and FIXMEs on <leader>T"
    git clone https://github.com/vim-scripts/TaskList.vim

    print_message "Install vim-buftabline - Navigate between opened files with tabs"
    git clone https://github.com/ap/vim-buftabline

    print_message "Install vim-signature - Place, toggle and display marks"
    git clone https://github.com/kshenoy/vim-signature

    print_message "Install commentary - Comment out: gcc to comment out a line, gc to a selection"
    git clone https://github.com/tpope/vim-commentary

    print_message "Install youcompleteme - As-you-type code completion, comprehension and refactoring engine"
    git clone https://github.com/valloric/youcompleteme

    print_message "Install tmuxline - tmux statusline"
    git clone https://github.com/edkolev/tmuxline.vim

    print_message "Install fugitive - Git in vim on :G"
    git clone https://github.com/tpope/vim-fugitive
}

create_directories
install_prerequisites
install_vim_plugins
