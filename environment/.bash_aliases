alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias lt='ls --human-readable --size -1 -S --classify'

alias cd..='cd ..'
alias ..='cd ..'

alias fox='firefox -ProfileManager -no-remote'
alias path='readlink -f'
alias grepp='grep -r -i -I -n --include=*\.{py,zts}'
alias grepa='grep -R -i -I -n --exclude=*\.log --exclude-dir=\.ctags --exclude-dir=\.git'
alias findd='find . -type f \( -iname \*.py -o -iname \*.zts \) | grep -i'
alias finda='find . | grep -i'
alias mvpn='/opt/cisco/anyconnect/bin/vpnui'
alias mssh='ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null"'
alias ztags="ctags --tag-relative=yes --langmap=python:+.zts -R -f .ctags ."

# Git
alias gs="git status"
alias gl="git lol"
alias gla="git lola"
alias gb="git branch"
alias gv="git branch -vv"
alias gd="git diff"

gmag () {
    CURRENT_BRANCH=$(git branch | grep \* | cut -d ' ' -f2)
    CMD1="git fetch origin"
    CMD2="git reset --hard origin/${CURRENT_BRANCH}"
    CMD3="git submodule update --init --recursive --force"
    echo $CMD1
    $CMD1
    echo $CMD2
    $CMD2
    echo $CMD3
    $CMD3
}
